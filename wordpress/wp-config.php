<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'B<^tN6}:yMu@J>)7F`qO$Wrpu#<to%-uE@V&vOPUJ&[E^mJNz5&`<j#Rl<>4S(dw' );
define( 'SECURE_AUTH_KEY',  'D`o6T_jl)sj}%lSPDN`gS_-^uRJC42}_i2N]KsCVs7D>K(qeQ7Hwf$~(jU243QF@' );
define( 'LOGGED_IN_KEY',    'm!k =GHGSyz=EuR~G/ubv!;V_;QE;Ko`4XKB.#kg/L,LKB+F-D;`G_5^WWIp41[O' );
define( 'NONCE_KEY',        '^(=[1J=;WEC1vlRswr~Ip)F[w,EDSFZNhD`zgS+qvG[dBBA?0C19E6Y6&&pO.&zQ' );
define( 'AUTH_SALT',        'y!3=2X4r;xo9/2li(*}b*xY^x%6e*|@tGV46kuggpGK(@3/Qd7@_-jw;TB:X5FZL' );
define( 'SECURE_AUTH_SALT', 'QZj?U)Ks-O^;AhnxWF@FtXqQf!=|ws1Zbg4],w<C,yywICSj-nM&s2&>vL>F0[,R' );
define( 'LOGGED_IN_SALT',   ';F Pt?Jk~XUn/W+$O/[iD~[<i},ig6)+%&)m~$b|!b1PkhE]nTM2*{;MT? WrxR&' );
define( 'NONCE_SALT',       '3>rTv5,*F3jSb>Z#Cd9AXO*,=>8rc,JAC(yf3HN!BH-=&nT#K_dbR=ol{$k`b]h/' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
