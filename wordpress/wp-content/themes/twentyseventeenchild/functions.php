<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}
function my_custom_sidebar() {
   register_sidebar(
       array (
           'name' => __( 'Custom', 'your-theme-domain' ),
           'id' => 'custom-side-bar',
           'description' => __( 'Custom Sidebar', 'your-theme-domain' ),
           'before_widget' => '<div class="widget-content">',
           'after_widget' => "</div>",
           'before_title' => '<h3 class="widget-title">',
           'after_title' => '</h3>',
       )
   );
}
add_action( 'widgets_init', 'my_custom_sidebar' );
function register_my_menu() {
    register_nav_menu('header-menu',__( 'Header Menu' ));
  }
  add_action( 'init', 'register_my_menu' );
?>
